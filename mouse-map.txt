MMB - Rotate the scene
SW - Zoom in and zoom out
Shift + MMB - Drag Panning i.e. moving the scene [HOLD]
RMB - Click to select an object
MMB - Drag in group of buttons in a window header to scroll through

Legend
------
LMB - Left Mouse Button
MMB - Middle Mouse Button
RMB - Right Mouse Button
SW - Scroll Wheel
HOLD - Hold the keys viz. do not release
