This chapter describes the blender interface for saving and loading files.

Timeline
--------

[0:35] : Saving and loading interface
[3:30] : Saving a blend file
[3:50] : Save on quit

Details
-------

The save and load interface editor contains buttons to create a new directory,
a group of buttons to change the file arrangement, a set of sort options for
the file listing and a set of filters to view, hide different file types.

The useful among these are the thumbnail view option which shows blender files
with a camera view render in the thumbnail. You can sort the files by name,
date created etc. The filters contain an option to hide blend files with
blend1, blend2 extensions which are previous versions of the blend file that
blender automatically saves. 

You can use these to revert to a previous version or use the Recover Auto Save
from the File menu which also contains a menu item to Recover Last Session in
case of a blender crash.

The interface editor also contains a bookmark button on the left panel which
you can use to add the current folder. This is useful for frequently used
folders to avoid redundant navigation.

In order to save, simply press Cmd+S. Remember that blender does not
automatically remind you to save on Quit, and you can change this via User
preferences setting.
