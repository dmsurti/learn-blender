Z - Toggle between Solid and Wireframe shading mode
Alt + Z - Toggle between Solid and Texture shading mode
Num0 - Camera View
Num1 - Front View
Ctrl + Num1 - Back View
Num3 - Right Side View
Ctrl + Num3 - Left Side View
Num7 - Top View
Ctrl + Num7 - Bottom View
Num5 - Toggle between orthographic and perspective projection
Shift + C - Center the scene (if lost after panning)
TAB - Toggle between object and edit modes
Ctrl + U - Save a new layout as a startup file
Cmd + N - New blend file or load startup file
Cmd + , - User preferences
T - Tools toolbar/region of 3D view window 
P - Properties toolbar of 3D view window (is different from Properties Window)
M - Move and LMB to move to a layer
Shift + LMB - In layers button to view multiple layers
~ - View all layers at the same time
0 to 9 (number row) - View the layer numbered 1 to 9, 0 for 10
Alt + 0 - 9 (number row) - View the layer numbered 10 to 19, 0 for 20
Shift + A - Add object
G - Translate (follwed by X, Y, Z to constrain to axis and use Ctrl for precise
increments; applies to R and S as well).
RMB - Select an object
RMB - Deslect a selected object
Shift + RMB - Select multiple objects (last selected object is orange, others
are darker shade)
Shift + RMB + RMB - Deslect multiple objects
R - Rotate
S - Scale
B - Box Border Select (LMB to select, MMB to deselect)
C - Circular Border Select (LMB to select, MMB to deselect)
Ctrl + I - Inverse selection (select unselected objects and vice versa)
A - Select All/Unselect All
H - Hide
Alt + H - Unhide
Ctrl + P - Parent last selected object as parent of first selected object
Ctrl + J - Join multiple objects into one
Shift + S - Snap cursor options
F6 - After adding an object before clicking LMB, you can see Add panel under
Tools. The options within this changes after adding with LMB. You can invoke
with F6.

Legend 
------ 
Num - Numpad 
Ctrl - Control 
Alt - Alt 
Cmd - Command 
HOLD - Hold the keys viz. do not release
